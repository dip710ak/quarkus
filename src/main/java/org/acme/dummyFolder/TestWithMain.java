package org.acme.dummyFolder;

import io.vertx.ext.web.multipart.impl.MultipartFormImpl;

public class TestWithMain {

	
	private static void aboutBooleanMain() {
		Boolean bool1 = true;
		boolean bool2 = true;
		
		if(bool1!=null) {
			System.out.printf("we can check the object of Boolean..., But we can't check the about he object state of the bool2...\n value is %s \n",bool1);
		}
		
		if(bool1) {
			System.out.println("we can also excetu logic block with it as like the boolean ...");
		}
		
	}
	
	public static void displayInfo() {
		
	}
	
	
	public static void main(String[] args) {
		aboutBooleanMain();
	}
	
}
