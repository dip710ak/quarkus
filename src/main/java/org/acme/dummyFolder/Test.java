package org.acme.dummyFolder;

import java.util.ArrayList;
import java.util.List;

public sealed class Test permits Dogs, Cats {
	public void show() {
		System.out.println("This is show in side the parent of sealed class...!!");
	}
}

final class Dogs extends Test {
	public void aboutLabedSwitch() {
//		its processd the sing value 
		int key = 1;

		String str = switch (key) {
		case 1 -> "one";
		case 2 -> "two";
		default -> throw new IllegalArgumentException("Unexpected value: " + key);
		};
		
		System.out.printf("Resulted value of the key : : %s, is %s.", key, str);
	}
}

final class Cats extends Test {
//	ist's all about the records ....
	public record Person(long id,String fname,String lname) {};
	
	
	public void info() {
		List<Person> list = new ArrayList<>();
		Person person1 = new Person(101, "xyz1","xyz1");
		Person person2 = new Person(102, "xyz2","xyz2");
		Person person3 = new Person(101, "xyz3","xyz3");
		list.add(person1);
		list.add(person2);
		list.add(person3);
		System.out.printf("The info we have is : : %s",list);
	}
	
}