package org.acme.controller;

import java.nio.file.Files;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.reactive.MultipartForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

@Path("/restEasyFile")
@ApplicationScoped
public class ReastEasyFileUpload {
	
	@POST
	@Path("/fileUp")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_PLAIN)
	public Response aboutFile(@MultipartForm FileUploadData file) {
		try {
			System.out.printf("about the File : : %s",file.toString());
			FileUpload file2 = file.getFile();
			System.out.printf("This fileName is : : %s, File Path : : %s , file Size : : %s ",file2.fileName(),file2.filePath(),file2.size());
			System.out.println("\nFile Contenet : : "+Files.readString(file2.filePath()));
		}catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("SomeThing Went Wrong plz try again...").build();
		}
		return Response.ok(String.format("Successfull  With %s ",file.getId())).build();
	}
	
}
