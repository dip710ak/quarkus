package org.acme.controller;

import org.acme.entity.Person;
import org.acme.serviceImpl.PersonImpl;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/person")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PersonInfoController {

	@Inject
	PersonImpl personImple;
	
	@GET
	@Path("/all")
	public Response getAllPerson() {
		System.out.println("INside the All person ....");
		List<Person> all = personImple.getAll();
		return Response.ok(all).build();
//		return Response.status(Status.OK).entity(all).build();
	}
	
	@POST
	@Path("/create")
	public Response createPerson(@Valid Person person){
		System.out.println("Inside the Create Person ..!!");
		Person per = personImple.createPerson(person);
		return Response.ok(per).build();
	}
	
	@DELETE
	@Path("/delete/{id}")
	public Response deleteEntity(@PathParam("id") String id) {
		System.out.println("Inside the delete....!!");
		List<Person> deletPerson = personImple.deletPerson(id);
		return Response.ok(deletPerson).build();
	}
	
	@PUT
	@Path("/update/{id}")
	public Response updateEntity(@PathParam("id")String id,@Valid Person person) throws Exception {
		Person updatePerson = personImple.updatePerson(id, person);
		return Response.ok(updatePerson).build();
	}
	
	
	@GET
	@Path("/getById/{id}")
	public Response getPersonById(@PathParam("id") String id) throws Exception {
		Person person = personImple.getById(id);
		return Response.ok(person).build();
	}
	
	

}
