package org.acme.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.reactive.MultipartForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

import io.quarkus.qute.Location;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;

@ApplicationScoped	
@Path("file.html")
@Produces(MediaType.TEXT_HTML)
public class FileController {
	
	/* @Location("./META-INF/resource/files.html") */
	Template template;
	
	@GET
	public TemplateInstance getInstance() {
		return template.instance();
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public TemplateInstance formData(@MultipartForm fileuploadInpu fil) throws IOException {
		
		System.out.println("File >> "+fil.text);
		System.out.println("File >> "+fil.file.getAbsolutePath());
		
		return null;
	}
	
	public static class fileuploadInpu{
		
		@FormParam("text")
		public String text;
		
		@FormParam("file")
		public File file;
	}
}



