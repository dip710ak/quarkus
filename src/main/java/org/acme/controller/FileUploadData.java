package org.acme.controller;

import javax.ws.rs.FormParam;

import org.jboss.resteasy.reactive.multipart.FileUpload;

public class FileUploadData {
	@FormParam("idno")
	private long id;
	@FormParam("fname")
	private String fname;
	@FormParam("lname")
	private String lname;
	@FormParam("fielName")
	private FileUpload file;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public FileUpload getFile() {
		return file;
	}
	public void setFile(FileUpload file) {
		this.file = file;
	}
	@Override
	public String toString() {
		return "FileUploadData [id=" + id + ", fname=" + fname + ", lname=" + lname + "]";
	}
	
	
	
	
}
