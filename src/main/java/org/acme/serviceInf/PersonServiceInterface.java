package org.acme.serviceInf;

import java.util.List;

import org.acme.entity.Person;

public interface PersonServiceInterface {
	public List<Person> getAll();
	public Person createPerson(Person person);
	public Person updatePerson(String id,Person person)throws Exception;
	public List<Person> deletPerson(String id);
	public Person getById(String id)throws Exception;
}
