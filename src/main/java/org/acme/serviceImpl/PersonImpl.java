package org.acme.serviceImpl;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Validator;

import org.acme.customException.CustomExcep;
import org.acme.entity.Person;
import org.acme.repo.PersonRepo;
import org.acme.serviceInf.PersonServiceInterface;

import io.quarkus.hibernate.orm.panache.PanacheQuery;

@ApplicationScoped
public class PersonImpl implements PersonServiceInterface{

	@Inject
	PersonRepo personRepo;
	
	@Inject
	Validator validator;
	
	@Override
	public List<Person> getAll() {
		PanacheQuery<Person> findAll = personRepo.findAll();
		List<Person> list = findAll.list();
		return list;
	}

	@Transactional
	public Person createPerson(Person person) {
		System.out.println("person __>"+person.toString());
		personRepo.persistAndFlush(person);	
		
//				manual validation using Validator Interface of package validation validator.....
//		
//		manual validation can be used for the advance validation of the incoming data.....
//		
		
//		Set<ConstraintViolation<Person>> validate = validator.validate(person);
//		if(validate.isEmpty()) {
//			personRepo.persist(person);
//			return person;
//		}else {
//			String collect = validate.stream().map(valid->valid.getMessage()).collect(Collectors.joining(", "));
//				return collect; // this melthod must be of Object Type ......
//		}	
		
		return person;
	}

	@Transactional
	@Override
	public Person updatePerson(String id,Person per) throws Exception {
		Person person = personRepo.findById(Long.parseLong(id));
		if(person==null) {
			throw new CustomExcep("No data to Updata...!");
		}
		person.setFname(per.getFname());
		person.setLname(per.getLname());
		personRepo.persist(person);
		return getById(id);
	}

	@Transactional
	@Override
	public List<Person> deletPerson(String id) {
		System.out.println("Inside the IMple File ...!!");
		personRepo.findAll().list().stream().filter(person->person.getId()==Long.parseLong(id)).findAny().ifPresent(person -> personRepo.delete(person));
		return getAll();
	}

	
	@Transactional
	@Override
	public Person getById(String id) throws Exception{
		
//		Person person = personRepo.findByIdOptional(Long.parseLong(id)).orElseThrow(()-> new CustomExcep(String.format("Not data with Id : : %s", id)));  //--->using the findByIdOptional(Long id)
//		Person person2 = personRepo.findByIdOptional(Long.parseLong(id)).stream().findFirst().orElseThrow(()-> new CustomExcep("not goons...!!"));
		
		Person person = personRepo.findById(Long.parseLong(id));
		if(person==null)
			throw new CustomExcep(String.format("No Data With Id : %s", id));
		return person;
	}

}
