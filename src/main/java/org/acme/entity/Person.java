package org.acme.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
public class Person {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotBlank
	@Pattern(regexp = "^[a-zA-Z]+$",message = "Must not be Combination of any Character like \'[]@!#$%^&*() \'..etc or numebr")
	@Size(min = 3,max = 10,message = "Size Must be Between 3 and 10...!!")
    private String fname;
	
	@NotBlank
	@Size(min = 3,max = 10,message = "Size Must be Between 3 and 10...!!")
    private String lname;
	
	@Pattern(regexp = "^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+$",message = "Please Enter a valid mail Id..")	
	private String mail;
	

	@Min(value = 15,message = "Age Must be Greater than 15.")
	@Max(value = 150,message = "Age Must Not be Greater than 150.")
	private int age;
	
	
    public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                '}';
    }
}
