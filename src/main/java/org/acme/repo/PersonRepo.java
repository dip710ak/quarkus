package org.acme.repo;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import org.acme.entity.Person;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@Transactional
@ApplicationScoped
public class PersonRepo implements PanacheRepository<Person>{
	
}
